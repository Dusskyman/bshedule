import 'package:Provider_template_v1/data/dto/login_dto.dart';
import 'package:Provider_template_v1/network/requests/auth/login_request.dart';

class AuthService {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  AuthService._privateConstructor();

  static final AuthService _instance = AuthService._privateConstructor();

  static AuthService get instance => _instance;

  // endregion

  Future<Map<String, dynamic>> loginWithEmailAndPassword(LoginDto dto) async {
    return await LoginRequest(dto: dto).send();
  }
}