import 'package:Provider_template_v1/data/dto/login_dto.dart';
import 'package:Provider_template_v1/network/shared/request.dart';

class LoginRequest extends Request<Map<String, dynamic>> {
  final LoginDto dto;

  LoginRequest({this.dto});

  @override
  Future<Map<String, dynamic>> send() async {
    // http request

    Map<String, dynamic> response = {
      'name': 'yura',
      'email': 'email',
      'phone': '123',
    };

    return response;
  }
}